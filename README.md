# b3-dev-tu-bourdaud-leon-jeremy-ssonyan

## Description

- This is an attempt of  creating a mini  calculator in a node project with mocha use for tdd and bdd

- Test-driven development (TDD) is a software development approach in which tests are written for a piece of code before the code is written. The tests are designed to fail initially, and then the code is written to pass the tests. This approach helps ensure that the code is correct and meets the requirements.

- In the context of testing arithmetic operations, TDD might involve writing a test that checks the result of an addition operation and then writing code to make the test pass.

- Behavior-driven development (BDD) is a software development approach that focuses on defining the behavior of a system or feature from the perspective of the user. It involves writing tests that describe how the system should behave in different scenarios, and then writing code to make the tests pass.

- In the context of testing arithmetic operations, BDD might involve writing tests that describe the expected behavior of the arithmetic operations from the perspective of the user.

## Installation

Clone the repo : git clone https://gitlab.com/lebourdaud/b3-dev-tu-leon-jeremy-ssonyan.git
Install express : npm install express
Install  nodemon (optional) : npm install nodemon 
Install mocha : npm install mocha

***

## How to deploy

You'll need to type <npm start> or <nodemon> in terminal  to start the project.
If you want to verify scenarios(operations), type <npm test> in terminal