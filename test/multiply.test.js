const expect = require('chai').expect;
const { Multiply } = require('../src/classes/Multiply');

describe('Testing the multiply function', function() {
    it("1. basic use", function(done){
        let calc1 = new Multiply(2,3);
        expect(calc1.getResult()).to.equal(6);
        done();
    });

    it("2. with zero", function(done){
        let calc2 = new Multiply(0,3);
        expect(calc2.getResult()).to.equal(0);
        let calc2_1 = new Multiply(0,0);
        expect(calc2_1.getResult()).to.equal(0);
        done();
    });

    it("3. with negatives", function(done){
        let calc3 = new Multiply(-2,3);
        expect(calc3.getResult()).to.equal(-6);
        let calc3_1 = new Multiply(-2,-3);
        expect(calc3_1.getResult()).to.equal(6);
    done();
    });

    it("4. with decimals", function(done){
        let calc4 = new Multiply(1.5,4);
        expect(calc4.getResult()).to.equal(6);
        let calc4_1 = new Multiply(0.5,3);
        expect(calc4_1.getResult()).to.equal(1.5);
        let calc4_2 = new Multiply(0.5,2.5);
        expect(calc4_2.getResult()).to.equal(1.25);
    done();
    });
});