const expect = require("chai").expect;
const { PercentageCalculating } = require("../src/classes/PercentageCalculating");


describe("Testing the percentage calculating method", function () {
    it("1. Calculates the percentage", function (done) {
      let operation = new PercentageCalculating(50, 100);
      expect(operation.getPercentage()).to.equal(50);
      done();
    });
    it("2. With total zero", function (done) {
      let operation = new PercentageCalculating(50, 0);
      expect(operation.getPercentage()).to.equal("Le total ne peut être zéro");
      done();
    });
  });