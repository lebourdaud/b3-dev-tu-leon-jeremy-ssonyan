const express = require('express');
const app = express();
const path = require('path');


//base route
app.get('/',function(req,res){
  res.sendFile(path.join(__dirname +'/index.html'));
});

//port
app.listen(process.env.port || 3000);

// style import
app.get('/style.css', (req, res) => {
  res.sendFile(__dirname + '/style.css');
});

// classes import
app.get('/classes/Addition.js', (req, res) => {
  res.sendFile(__dirname + '/classes/Addition.js');
});
app.get('/classes/Subbstraction.js', (req, res) => {
  res.sendFile(__dirname + '/classes/Subbstraction.js');
});
app.get('/classes/Multiply.js', (req, res) => {
  res.sendFile(__dirname + '/classes/Multiply.js');
});
app.get('/classes/Divide.js', (req, res) => {
  res.sendFile(__dirname + '/classes/Divide.js');
});
app.get('/classes/PercentageCalculating.js', (req, res) => {
  res.sendFile(__dirname + '/classes/PercentageCalculating.js');
});
app.get('/classes/Power.js', (req, res) => {
  res.sendFile(__dirname + '/classes/Power.js');
});
app.get('/classes/SquarePower.js', (req, res) => {
  res.sendFile(__dirname + '/classes/SquarePower.js');
});
app.get('/classes/SquareRoot.js', (req, res) => {
  res.sendFile(__dirname + '/classes/SquareRoot.js');
});

console.log('Running at Port 3000');
