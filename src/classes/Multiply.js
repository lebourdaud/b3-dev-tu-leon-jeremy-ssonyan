/* Feature: multiplication calculation 

  Scenario: calculating the multiplication of 2 number
    Given the value 50 and multiplying it by 2
    When I calculate it
    Then the result should be 100
*/

// Multiply a number with an other
class Multiply{
    constructor(oldvalor,newvalor) {
    this.oldvalor = oldvalor;
    this.newvalor = newvalor;
    }

    getResult () {
        let waitingvalor = this.newvalor;
        let newvalor = this.oldvalor*this.newvalor;
        this.oldvalor = waitingvalor;
        return newvalor
    }
}

module.exports = {
    Multiply: Multiply,
};