/* Feature: Subbstraction calculation 

  Scenario: calculating the subbstraction of 2 numbers
    Given the value 50 and the other 50
    When I do the subbstraction
    Then the result should be 0
*/

// Calculate subbstraction of 2 numbers
class Subbstraction {

    constructor(a,b) {
        this.a = a;
        this.b = b;
    }

    subbstract () {
        return this.a-this.b;
    }
}

module.exports = {
    Subbstraction: Subbstraction,
};